# pipeline

## Debianization Steps (Overview)
1.  Use the appropriate 'init_from*' file to pull the tar down (More details below)
2.  Enter into new dir and run the packaging tool: 'debianize' with the package name as an argument
3.  Make appropriate changes to the newly created debain files in the debian folder
4.  Edit setting within Gitlab repo page. Enter `debian/.gitlab-ci.yml` into 'custom ci path' in settings>CI/CD>general pipeline
4.  Run the tool: 'finish_repo' to make a pristine tar commit and complete upload
5.  Pipeline should have started. Watch for results.

## Tool Files

### init_*
This series of files are labelled:
*  init_repo
*  init_from_gitlink
*  init_from_tar
*  init_from_tarlink

These are used to download a tar file/source code, untar, and then initialize a repo in 
TwoSix-Kali/packages. Init_repo contains the nessisary steps.

The init_from* files just use the appropriate method for pulling the source code. 
Init_from files then send the work to init_repo.

Usage example:
`init_from_gitlink <http://link.to.github/repo.git> <version>`

### debianize
Used to create /debian/ within the source code directory. 

Also attempts to create the orig tarball if it doesn't exist. This is usefull 
if the orig tarball should be remade or is incorrectly named. Just delete the old 
before you run debianize.

### finish_repo
Completes the processing by committing pristine tar, and pushing to repo to start pipeline.

### add_depends
Simplifies the dependency process by finding the line that is supposed to have the 
dependencies added and adds the arguments to the file there. Allows for further automation.

### add_debs_to_debs
Downloads all links inside specified file to the debs/ folder.

### add_debs_from_file
Downloads and uploads the .deb specified inside of the file given in arg1. Uploads to the docker repository.

### public.py
Sets all of the source repo packages to public access. Used for add_debs_* file.