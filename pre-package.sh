#!/bin/bash
## should be only be run if their is no debian folder (new project)
## creates the files and dirs needed for simple packaging
## regex format: name-version.tar.gz
## use standard email format

if [ "$#" -ne 2 ]; then
	echo "ERROR. USAGE: ./packaging.sh [name-version.tar.gz] [email]"
	exit 2
fi
FILENAME=$(echo $1 | sed -rn 's/([a-zA-Z]+)-(.+).tar.gz/\1_\2.orig.tar.gz/gp')
if [ "${FILENAME}" == "" ]; then
	echo "ERROR. USAGE: ./packaging.sh [name-version.tar.gz] [email]"
	exit 2
fi

PROGNAME=$(echo $1 | sed -rn 's/([a-zA-Z]+)-(.+).tar.gz/\1-\2/gp')
VERSNAME=$(echo $1 | sed -rn 's/([a-zA-Z]+)-(.+).tar.gz/\2/gp')
NAMENAME=$(echo $1 | sed -rn 's/([a-zA-Z]+)-(.+).tar.gz/\1/gp')
echo "New filename: ${FILENAME}"
echo "Program: ${PROGNAME}"
echo "Version: ${VERSNAME}"
echo "Name: ${NAMENAME}"

mv $1 ${FILENAME}

mkdir ${PROGNAME}
tar xf ${FILENAME} -C ${PROGNAME} --strip-components=1

cd ${PROGNAME}

mkdir debian




## Running dch

EMAIL=$(echo $2 | sed -rn 's/([a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})/\1/gp')
if [ "${EMAIL}" == "" ]; then
	echo "ERROR. Email format incorrect/unsupported."
	exit 2
fi	
export DEBEMAIL=$2

dch --create -v ${VERSNAME}-1 --package ${NAMENAME} "Initial build."


## Functionality to auto edit file here?

## compat file
echo "10" > debian/compat

## control file
echo -e "Source: ${NAMENAME}
Maintainer: name <name@email.com>
Section: misc
Priority: optional
Standards-Version: 3.9.2
Build-Depends: debhelper (>= 9)

Package: ${NAMENAME}
Architecture: any
Depends: \${shlibs:Depends}, \${misc:Depends}
Description: short one
 Insert longer description here.
" > debian/control

## copyright file
echo "" > debian/copyright

## rules file
echo -e '#!/usr/bin/make -f
%:
\tdh $@
' > debian/rules
chmod +x debian/rules

## format file
mkdir debian/source
echo "3.0 (quilt)" > debian/source/format

## kali configs
echo "[DEFAULT]
debian-branch = kali/master
debian-tag = kali/%(version)s
pristine-tar = True

[pq]
patch-numbers = False

[dch]
multimaint-merge = True
" > debian/gbp.conf

echo "include:
  - https://gitlab.com/kalilinux/tools/kali-ci-pipeline/raw/master/recipes/kali.yml
" > debian/kali-ci.yml

echo -e 'extend-diff-ignore = "^[^/]*[.]egg-info/"' > debian/source/options


echo "Ready to build."

